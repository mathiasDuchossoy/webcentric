<?php

namespace App\Controller;

use App\Entity\DateChoice;
use App\Entity\Survey;
use App\Form\SurveyType;
use App\Service\SurveyService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SurveyController extends AbstractController
{
    private EntityManagerInterface $em;
    private SurveyService $surveyService;

    public function __construct(EntityManagerInterface $em, SurveyService $surveyService)
    {
        $this->em = $em;
        $this->surveyService = $surveyService;
    }

    /**
     * @Route("/survey", name="survey")
     */
    public function index(): Response
    {
        return $this->render('survey/index.html.twig', [
            'controller_name' => 'SurveyController',
        ]);
    }

    /**
     * @Route("/survey/new", name="survey_new")
     */
    public function new(Request $request, SurveyService $surveyService): Response
    {
        // just setup a fresh $survey object (remove the example data)
        $survey = new Survey();

        $form = $this->createForm(SurveyType::class, $survey);
        $form->add('save', SubmitType::class, ['label' => 'Create Survey']);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $survey->setUrl($surveyService->randomUrl());

            $this->em->persist($survey);
            $this->em->flush();

            return $this->redirectToRoute('survey_list');
        }

        return $this->render('survey/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/survey/{url}/edit", name="survey_edit")
     */
    public function edit(string $url, Request $request): Response
    {
        $survey = $this->surveyService->getSurvey($url);


        $originalDateChoices = new ArrayCollection();

        // Create an ArrayCollection of the current DateChoice objects in the database
        foreach ($survey->getDateChoices() as $dateChoice) {
            $originalDateChoices->add($dateChoice);
        }


        $form = $this->createForm(SurveyType::class, $survey);
        $form->add('save', SubmitType::class, ['label' => 'Edit Survey']);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($originalDateChoices as $dateChoice) {
                if (false === $survey->getDateChoices()->contains($dateChoice)) {
                    $survey->removeDateChoice($dateChoice);
                    $this->em->remove($dateChoice);
                }
            }

            $this->em->flush();

            return $this->redirectToRoute('survey_edit', ['url' => $url]);
        }

        return $this->render('survey/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/survey/list", name="survey_list")
     */
    public function list(): Response
    {
        $surveys = $this->em->getRepository(Survey::class)->findAll();

        return $this->render('survey/list.html.twig', [
            'surveys' => $surveys,
        ]);
    }

    /**
     * @Route("/survey/show/{url}", name="survey_show")
     */
    public function show(string $url): Response
    {
        try {
            $survey = $this->surveyService->getSurvey($url);


            $iterator = $survey->getDateChoices()->getIterator();
            $iterator->uasort(function (DateChoice $a, DateChoice $b) {
                return (count($a->getIndividuals()) > count($b->getIndividuals())) ? -1 : 1;
            });
            $dateChoices = new ArrayCollection(iterator_to_array($iterator));

            $survey->setDateChoices($dateChoices);
        } catch (EntityNotFoundException $exception) {
            return $this->redirectToRoute("survey_list");
        }

        return $this->render('survey/show.html.twig', [
            'survey' => $survey,
        ]);
    }

    /**
     * @Route("/survey/delete/{url}", name="survey_delete")
     */
    public function delete(string $url): Response
    {
        try {
            $survey = $this->surveyService->getSurvey($url);

            $this->em->remove($survey);
            $this->em->flush();
            $this->addFlash('success', 'Survey delete.');

        } catch (EntityNotFoundException $exception) {
            $this->addFlash('error', $exception->getMessage());
        }

        return $this->redirectToRoute("survey_list");
    }
}
