<?php

namespace App\Controller;

use App\Entity\Individual;
use App\Entity\Survey;
use App\Form\IndividualType;
use App\Form\SurveyType;
use App\Service\SurveyService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndividualController extends AbstractController
{
    private EntityManagerInterface $em;
    private SurveyService $surveyService;

    public function __construct(EntityManagerInterface $em, SurveyService $surveyService)
    {
        $this->em = $em;
        $this->surveyService = $surveyService;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        $surveys = $this->em->getRepository(Survey::class)->findAll();

        return $this->render('individual/index.html.twig', [
            'surveys' => $surveys,
        ]);
    }

    /**
     * @Route("/individual/survey/{url}/new", name="individual_new")
     */
    public function new( string $url, Request $request): Response
    {
        $individual = new Individual();

        $survey = $this->surveyService->getSurvey($url);
        $form = $this->createForm(IndividualType::class, $individual, [
            'survey' => $survey
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->em->persist($individual);
            $this->em->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('individual/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
