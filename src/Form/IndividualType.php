<?php

namespace App\Form;

use App\Entity\DateChoice;
use App\Entity\Individual;
use App\Entity\Survey;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IndividualType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dateChoices = [];
        /** @var Survey $survey */
        if (null !== $survey = $options['survey']) {
            $dateChoices = $survey->getDateChoices();
        }

        $builder
            ->add('firstName')
            ->add('dateChoices' , EntityType::class, [
                'class' => DateChoice::class,
                'multiple' => true,
                'expanded' => true,
                'choices' => $dateChoices
            ])
            ->add('save', SubmitType::class, ['label' => 'Save']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Individual::class,
            'survey' => null
        ]);
    }
}
