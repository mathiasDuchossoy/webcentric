<?php


namespace App\Service;


use App\Entity\Survey;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class SurveyService
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function randomUrl(): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        return substr(str_shuffle($chars), 0, 8);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function getSurvey(string $url): Survey
    {
        if (null === $survey = $this->em->getRepository(Survey::class)->findOneByUrl($url)) {
            throw new EntityNotFoundException("Survey not found for url : $url.");
        }

        return $survey;
    }
}
