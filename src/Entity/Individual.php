<?php

namespace App\Entity;

use App\Repository\IndividualRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IndividualRepository::class)
 */
class Individual
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\ManyToMany(targetEntity=DateChoice::class, inversedBy="individuals", cascade={"persist"})
     */
    private $dateChoices;

    public function __construct()
    {
        $this->dateChoices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return Collection|DateChoice[]
     */
    public function getDateChoices(): Collection
    {
        return $this->dateChoices;
    }

    public function addDateChoice(DateChoice $dateChoice): self
    {
        if (!$this->dateChoices->contains($dateChoice)) {
            $this->dateChoices[] = $dateChoice;
            $dateChoice->addIndividual($this);
        }

        return $this;
    }

    public function removeDateChoice(DateChoice $dateChoice): self
    {
        if ($this->dateChoices->removeElement($dateChoice)) {
            $dateChoice->removeIndividual($this);
        }

        return $this;
    }
}
