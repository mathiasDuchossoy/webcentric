<?php

namespace App\Entity;

use App\Repository\DateChoiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DateChoiceRepository::class)
 */
class DateChoice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToMany(targetEntity=Individual::class, mappedBy="dateChoices", cascade={"remove"})
     */
    private $individuals;

    /**
     * @ORM\ManyToOne(targetEntity=Survey::class, inversedBy="dateChoices")
     */
    private $survey;

    public function __construct()
    {
        $this->individuals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|Individual[]
     */
    public function getIndividuals(): Collection
    {
        return $this->individuals;
    }

    public function addIndividual(Individual $individual): self
    {
        if (!$this->individuals->contains($individual)) {
            $this->individuals[] = $individual;
        }

        return $this;
    }

    public function removeIndividual(Individual $individual): self
    {
        $this->individuals->removeElement($individual);

        return $this;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }

    public function setSurvey(?Survey $survey): self
    {
        $this->survey = $survey;

        return $this;
    }

    public function __toString(): string
    {
        return $this->date->format('m/d/Y');
    }

    public function nbVote(): int
    {
        return count($this->individuals);
    }
}
