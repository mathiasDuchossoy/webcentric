<?php

namespace App\Entity;

use App\Repository\SurveyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SurveyRepository::class)
 */
class Survey
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $url;

    /**
     * @ORM\OneToMany(targetEntity=DateChoice::class, mappedBy="survey", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private Collection $dateChoices;

    public function __construct()
    {
        $this->dateChoices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Collection|DateChoice[]
     */
    public function getDateChoices(bool $orderByDate = false): Collection
    {
        if (false !== $orderByDate) {
            $criteria = Criteria::create()
                ->orderBy(['date' => Criteria::ASC]);

            return $this->dateChoices->matching($criteria);
        }
        return $this->dateChoices;
    }

    public function setDateChoices(ArrayCollection $dateChoices): self
    {
        $this->dateChoices = $dateChoices;

        return $this;
    }

    public function addDateChoice(DateChoice $dateChoice): self
    {
        if (!$this->dateChoices->contains($dateChoice)) {
            $this->dateChoices[] = $dateChoice;
            $dateChoice->setSurvey($this);
        }

        return $this;
    }

    public function removeDateChoice(DateChoice $dateChoice): self
    {
        if ($this->dateChoices->removeElement($dateChoice)) {
            // set the owning side to null (unless already changed)
            if ($dateChoice->getSurvey() === $this) {
                $dateChoice->setSurvey(null);
            }
        }

        return $this;
    }

    public function nbVote()
    {
        $i = 0;
        /** @var DateChoice $dateChoice */
        foreach ($this->dateChoices as $dateChoice) {
            $i += count($dateChoice->getIndividuals());

        }
        return $i;
    }
}
