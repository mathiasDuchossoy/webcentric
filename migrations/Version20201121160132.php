<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201121160132 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE date_choice (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, survey_id INTEGER DEFAULT NULL, date DATE NOT NULL)');
        $this->addSql('CREATE INDEX IDX_251F6571B3FE509D ON date_choice (survey_id)');
        $this->addSql('CREATE TABLE individual (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE individual_date_choice (individual_id INTEGER NOT NULL, date_choice_id INTEGER NOT NULL, PRIMARY KEY(individual_id, date_choice_id))');
        $this->addSql('CREATE INDEX IDX_A853783BAE271C0D ON individual_date_choice (individual_id)');
        $this->addSql('CREATE INDEX IDX_A853783BA6F05565 ON individual_date_choice (date_choice_id)');
        $this->addSql('CREATE TABLE survey (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE date_choice');
        $this->addSql('DROP TABLE individual');
        $this->addSql('DROP TABLE individual_date_choice');
        $this->addSql('DROP TABLE survey');
    }
}
