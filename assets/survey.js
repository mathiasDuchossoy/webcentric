const $ = require('jquery');


$(document).ready(function() {
    // Get the ul that holds the collection of dateChoices
    var $dateChoicesCollectionHolder = $('ul.dateChoices');
    
    // add a delete link to all of the existing dateChoice form li elements
    $dateChoicesCollectionHolder.find('li').each(function() {
        addDateChoiceFormDeleteLink($(this));
    });

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $dateChoicesCollectionHolder.data('index', $dateChoicesCollectionHolder.find('li').length);

    $('body').on('click', '.add_item_link', function(e) {
        var $collectionHolderClass = $(e.currentTarget).data('collectionHolderClass');
        // add a new dateChoice form (see next code block)
        addFormToCollection($collectionHolderClass);
    })
});

function addFormToCollection($collectionHolderClass) {
    // Get the ul that holds the collection of dateChoices
    var $collectionHolder = $('.' + $collectionHolderClass);

    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your dateChoices field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a dateChoice" link li
    var $newFormLi = $('<li></li>').append(newForm);
    // Add the new form at the end of the list
    $collectionHolder.append($newFormLi)
    addDateChoiceFormDeleteLink($newFormLi);
}

function addDateChoiceFormDeleteLink($dateChoiceFormLi) {
    var $removeFormButton = $('<button type="button">Delete this dateChoice</button>');
    $dateChoiceFormLi.append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        // remove the li for the dateChoice form
        $dateChoiceFormLi.remove();
    });
}
