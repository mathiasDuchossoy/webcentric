# Webcentric technical test

## Installation

Install composer and yarn

```bash
composer install
yarn install
yarn build
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

## Usage

You can navigate everywhere by the home page ('/')

There is an admin link on the top

or you can choose date in the table


The database is with sqlite.

